import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Storage } from '@ionic/storage';
import * as Constant from '../../app/constants'
import { Observable } from 'rxjs/Observable';
import { Platform } from 'ionic-angular';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { LinkedIn } from '@ionic-native/linkedin';
import { GooglePlus } from '@ionic-native/google-plus';

// Do not import from 'firebase' as you'll lose the tree shaking benefits
import firebase from 'firebase/app';

export class User {
	name: string;
	email: string;

	constructor(name: string, email: string) {
		this.name = name;
		this.email = email;
	}
}

@Injectable()
export class LoginService {
	// currentUser: User;

	private webClientId = '519364484616-6p8igu96riuc8d47bviul3c7bailj8tm.apps.googleusercontent.com';
	private currentUser: firebase.User;

	constructor(
		private http: Http, 
		private storage: Storage, 
		private linkedin: LinkedIn,
		private googlePlus : GooglePlus,
		private platform : Platform
	) {

		if(platform.is('android')){
			this.webClientId = '519364484616-239kc1d3s2ahs1ugr2ci119j4gpoqd6g.apps.googleusercontent.com';
		}

	}

	get authenticated(): boolean {
		return this.currentUser !== null;
	}

	loginWithEmail(credentials): Observable<any> {
		let apiUrl = Constant.API_URL + '/signup-with-email';
		let headers = new Headers({ 'content-type': 'application/json' });
		let options = new RequestOptions({
			method: 'POST',
			headers: headers,
			body: { email: credentials.email }
		});
		return this.http
			.get(apiUrl, options)
			.map((res: Response) => res.json())
			.catch((error: any) => Observable.throw(error.json().error || 'Server error'));
	}

	verifyEmail(credentials): Observable<any> {
		let apiUrl = Constant.API_URL_V2 + '/verify-email';
		let headers = new Headers({ 'content-type': 'application/json' });
		let options = new RequestOptions({
			method: 'POST',
			headers: headers,
			body: { "email": credentials.email, "verificationCode": credentials.code }
		});
		return this.http
			.get(apiUrl, options)
			.map((res: Response) => res.json())
			.catch((error: any) => Observable.throw(error.json().error || 'Server error'));
	}

	public loginWithGoogle(token) {
		let apiUrl = Constant.API_URL + '/v2/signup-with-google';
		let headers = new Headers({ 'Access-Token': token });
		let options = new RequestOptions({ headers: headers });
		return this.http
			.get(apiUrl, options)
			.map((res: Response) => res.json())
			.catch((error: any) => Observable.throw(error.json().error || 'Server error'));
	}

	signInWithGoogle() {
		return this.googlePlus.login({
			'webClientId': this.webClientId,
			'offline': true
		});
	}

	signInWithTwitter() {
		return firebase.auth().signInWithRedirect(new firebase.auth.TwitterAuthProvider());
	}

	public loginWithTwitter(token, secret) {
		let apiUrl = Constant.API_URL_V2 + '/login-with-twitter';
		let headers = new Headers({ 'access-token': token, 'access-secret': secret });
		let options = new RequestOptions({ headers: headers });
		return this.http
			.get(apiUrl, options)
			.map((res: Response) => res.json())
			.catch((error: any) => Observable.throw(error.json().error || 'Server error'));
	}

	public loginWithLinkedin(token) {
		let apiUrl = Constant.API_URL_V2 + '/login-with-linkedIn';
		let headers = new Headers({ 'access-token': token });
		let options = new RequestOptions({ headers: headers });
		return this.http
			.get(apiUrl, options)
			.map((res: Response) => res.json())
			.catch((error: any) => Observable.throw(error.json().error || 'Server error'));
	}

	public logout() {
		let ls = this;
		firebase.auth().signOut().then(function () {
			ls.storage.clear().then(() => {
				return true;
			});
		}, function (error) {
			console.error('Sign Out Error', error);
		});
		this.linkedin.logout();
	}
}
