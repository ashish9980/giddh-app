import { Component } from '@angular/core';
import { NavController, Loading, AlertController, MenuController } from 'ionic-angular';
import { LoginService } from './login-service';
import { companyService } from "../company/company.service";
import { Storage } from '@ionic/storage';

import { TBalance } from '../tbalance/tbalance';

import { Common } from '../../shared/common';
import { SessionService } from "../../shared/session.service";
import { InfoService } from "../../shared/info.service";
import firebase from 'firebase';
import { LinkedIn } from '@ionic-native/linkedin';

@Component({
	selector: 'page-login',
	templateUrl: 'login.html',
	providers: [companyService, LoginService, Common]
})

export class LoginPage {
	loading: Loading;
	codeAvailable: boolean = false;
	registerCredentials = { email: '', code: '' };
	userProfile: any = null;
	token: any = null;

	constructor(
		private navCtrl: NavController,
		private loginService: LoginService,
		private companyService: companyService,
		private storage: Storage,
		public common: Common,
		public sessionService: SessionService,
		public infoService: InfoService,
		private linkedin: LinkedIn,
		private alert : AlertController,
		private menu : MenuController
	) {
		storage.get('sessionId').then((val) => {
			if (val) {
				console.log(val);
				this.logout();
			}
		});
	}

	ionViewDidEnter(){
		if(this.menu){
			this.menu.swipeEnable(false);
		}
	}

	ionViewWillLeave(){
		if(this.menu){
			this.menu.swipeEnable(true);
		}
	}

	public loginWithEmail() {


		console.log(this.registerCredentials);

		if (!this.registerCredentials.code) {

			console.log("Code not exists........");

			this.loginService.loginWithEmail(this.registerCredentials).subscribe(
				res => {
					if (res.status == 'success') {
						this.codeAvailable = true;
						console.log(res);
						this.common.showMsg(res.status, res.body);
					}
				},
				err => {
					console.log(err);
				});
		}
		else {
			console.log("Code exisrts.....");
			this.verifyEmail(this.registerCredentials);
		}
	}

	public setDefaultCompany(uniqueName, sessionId) {
		this.companyService.getCompanyList(uniqueName, sessionId).subscribe(
			res => {
				this.storage.set('company', res.body[0].uniqueName);
				this.storage.set('companyName', res.body[0].name);
				this.infoService.setCompany(res.body[0].name);
				this.navCtrl.setRoot(TBalance);
			},
			err => {
				console.log('error3');
				this.common.showMsg('error3', JSON.stringify(err));
			});
	}

	public verifyEmail(registerCredentials) {
		this.loginService.verifyEmail(registerCredentials).subscribe(
			res => {
				//save authkey and user
				this.sessionService.setSession(res.body.session.id);
				//this.infoService.setCompany(res.body.user.name);
				this.storage.set('sessionId', res.body.session.id);
				this.storage.set('user', res.body.user);
				this.infoService.setUserEmail(res.body.user.email);
				//set first company as default
				this.setDefaultCompany(res.body.user.uniqueName, res.body.session.id);
			},
			err => {
				this.common.showMsg('Error', 'Invalid Verification Code');
				console.log('err', err);
			});
	}

	public loginWithGoogle() {
		var lp = this;
		this.loginService.signInWithGoogle().then((res) => {

			this.token = res.accessToken;
			const googleCredential = firebase.auth.GoogleAuthProvider.credential(res.idToken);
			firebase.auth().signInWithCredential(googleCredential).then((res)=>{
				this.userProfile = res;
				// this.token = res.getToken();
				this.googleLoginSuccess();
			},(err)=>{
				this.alert.create({title : err}).present();
				console.log(err);
			});

			// firebase.auth().getRedirectResult().then(function (res) {
			// 	if (res) {
			// 		console.log("getRedirectResult");
			// 		lp.userProfile = res.user;
			// 		if (res.credential) {
			// 			lp.token = res.credential.accessToken;
			// 			lp.googleLoginSuccess();
			// 		}
			// 	}
			// });
		},(err)=>{
			this.alert.create({title : err}).present();
			console.log(err);
		});
	}

	public googleLoginSuccess() {
		var gs = this;
		gs.loginService.loginWithGoogle(this.token).subscribe(
			res => {
				if (res.status == 'success') {
					try{
						this.storage.set('user', res.body.user);
						this.storage.set('sessionId', res.body.session.id);
						this.sessionService.setSession(res.body.session.id);
						this.infoService.setUserEmail(res.body.user.email);
						this.setDefaultCompany(res.body.user.uniqueName, res.body.session.id);
					}catch(e){
						this.alert.create({title : ' Exception <--- 2'}).present();
					}
				}else {
					console.log("loginWithGoogleFailure: " + res);
				}
			},
			err => {
				console.log('error2');
				this.common.showMsg('error2', JSON.stringify(err));
				this.loginService.logout();
			});
	}

	public loginWithTwitter() {
		var lp = this;
		this.loginService.signInWithTwitter().then(function () {
			firebase.auth().getRedirectResult().then(function (res) {
				if (res) {
					lp.userProfile = res.user;
					console.log("getRedirectResult twitter: " + res.user.displayName);
					if (res.credential) {
						console.log("getRedirectResult twitter res.credential: " + res.credential.accessToken);
						lp.twitterLoginSuccess(res.credential.accessToken, res.credential.secret);
					}
				}
			}).catch(function (err) {
				console.error("Error getRedirectResult: " + err);
				// Handle Errors here.
				// var errorCode = err.code;
				// var errorMessage = err.message;
				//
				// // The email of the user's account used.
				// var email = err.email;
				// // The firebase.auth.AuthCredential type that was used.
				// var credential = err.credential;
				// console.error('errorCode: ' + errorCode + ' errorMessage: ' + errorMessage + ' email: ' + email + ' credentials: ' + credential);
			});;
		});
	}

	public twitterLoginSuccess(accessToken, secret) {
		var gs = this;
		gs.loginService.loginWithTwitter(accessToken, secret).subscribe(
			res => {
				if (res.status == 'success') {
					console.log("loginWithTwitter: " + res);
					this.storage.set('user', res.body.user);
					this.storage.set('sessionId', res.body.session.id);
					this.sessionService.setSession(res.body.session.id);
					this.infoService.setUserEmail(res.body.user.email);
					this.setDefaultCompany(res.body.user.uniqueName, res.body.session.id);
				}
				else {
					console.log("loginWithTwitterFailure: " + res);
				}
			},
			err => {
				console.log('error2');
				this.common.showMsg('error2', JSON.stringify(err));
				this.loginService.logout();
			});
	}

	public signInWithLinkedIn() {
		var ls = this;
		this.linkedin.hasActiveSession().then(function (session) {
			if (session) {
				ls.linkedin.getActiveSession().then(function (res1) {
					console.log("Linkedin res: " + res1.accessToken);
					if (res1.accessToken) {
						ls.loginService.loginWithLinkedin(res1.accessToken).subscribe(
							res2 => {
								if (res2.status == 'success') {
									console.log("signInWithLinkedIn: " + res2);
									ls.storage.set('user', res2.body.user);
									ls.storage.set('sessionId', res2.body.session.id);
									ls.sessionService.setSession(res2.body.session.id);
									ls.infoService.setUserEmail(res2.body.user.email);
									ls.setDefaultCompany(res2.body.user.uniqueName, res2.body.session.id);
								}
								else {
									console.log("signInWithLinkedIn: " + res2);
								}
							},
							err2 => {
								console.log('error2');
								ls.common.showMsg('error2', JSON.stringify(err2));
								ls.loginService.logout();
							});
					}
				})
					.catch()
			} else {
				ls.linkedin.logout();
				ls.linkedin.login(['r_basicprofile', 'r_emailaddress'], true)
					.then((res3) => {
						console.log(res3);
						console.log("Linkedin login access token: " + res3.accessToken);
						if (res3.accessToken) {
							ls.loginService.loginWithLinkedin(res3.accessToken).subscribe(
								res4 => {
									if (res4.status == 'success') {
										console.log("signInWithLinkedIn: " + res4);
										ls.storage.set('user', res4.body.user);
										ls.storage.set('sessionId', res4.body.session.id);
										ls.sessionService.setSession(res4.body.session.id);
										ls.infoService.setUserEmail(res4.body.user.email);
										ls.setDefaultCompany(res4.body.user.uniqueName, res4.body.session.id);
									}
									else {
										console.log("signInWithLinkedIn: " + res4);
									}
								},
								err3 => {
									console.log('error3');
									ls.common.showMsg('error3', JSON.stringify(err3));
									ls.loginService.logout();
								});
						}
					})
					.catch(e => console.log('Error logging in', e));
			}
		});
	}

	public logout() {
		this.loginService.logout();
	}

}
