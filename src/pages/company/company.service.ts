import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import * as Constant from '../../app/constants'

@Injectable()
export class companyService {       
    
    constructor( 
        private http: Http        
    ) {}

    getCompanyList(userUniqueName, sessionId): Observable<any>{
        let apiUrl = Constant.API_URL+'/users/'+userUniqueName+'/companies';        
        return this.http
               .get(apiUrl)
               .map((res:Response) => res.json())               
               .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }
}