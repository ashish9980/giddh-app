import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { companyService } from "./company.service";
import { TBalance } from '../../pages/tbalance/tbalance';
import { InfoService } from "../../shared/info.service";
/**
 * Generated class for the CompaniesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-company',
  templateUrl: 'company.html',
  providers: [companyService]
})
export class Company {  
  companies: any;
  uniqueName: string;
  authKey: string;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private companyService: companyService,
    private storage: Storage,
    private infoService: InfoService
  ) {
  }

  ngOnInit(): void {
    this.getCompanyList();
  }

  getCompanyList(): void {
    this.storage.get('user').then((user) => {
      this.uniqueName = user.uniqueName;
      this.storage.get('sessionId').then((key) => {
        this.authKey = key;        
        this.companyService.getCompanyList(this.uniqueName, this.authKey).subscribe(
          res => {
            this.companies = res.body;
          },
          err => {
              console.log(err);
        });
      });
    });   
  }

  switchCompany(company){
    this.storage.set('company', company.uniqueName);
    this.storage.set('companyName', company.name);
    this.infoService.setCompany(company.name);
    this.navCtrl.setRoot(TBalance);
  }

}
