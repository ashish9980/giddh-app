import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import * as Constant from '../../app/constants'

import { Observable }     from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class TBalanceService{           
    public company: string;    
    constructor(private http: Http) {}

    getTB(company, sessionId): Observable<any>{
        let apiUrl = Constant.API_URL+'/company/'+company+'/trial-balance'
        return this.http
            .get(apiUrl)
            .map((res:Response) => res.json())
    }
}

/*
Copyright 2017 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/