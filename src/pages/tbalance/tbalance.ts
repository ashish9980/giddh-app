import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import * as Constant from '../../app/constants'

import { TBalanceService } from "./tbalance.service";

@Component({
  selector: 'page-home',
  templateUrl: 'tbalance.html',  
  providers: [TBalanceService]
})

export class TBalance {
  accounts: any;  
  groups: any;
  total: number;
  groupsAndAccounts: any;
  searching: boolean = false;
  company: string;
  companyName: string;
  sessionId: any;
  
  //flatten
  flatten = [];
  category: string = '';
  sundrycreditors: boolean = false;
  sundrydebtors: boolean = false;

  //sorting
  columnGroup: String = '';
  columnAccount: String = '';
  direction: String = '';//desc  

  //breadcrumb
  previous: any;
  current: any;

  //chart
  chartData: any;

  constructor(
    private tBalanceService: TBalanceService,
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public loadingCtrl: LoadingController
  ) {    
  }

  getTB(): void {
    this.storage.get('company').then((company) => {
      this.company = company;
      this.storage.get('sessionId').then((sessionId) => {
        this.sessionId = sessionId;
        this.tBalanceService.getTB(this.company, this.sessionId).subscribe(
          res => {
            console.log(res.body);
            this.groupsAndAccounts = res.body;        
            this.accounts = (res.body.accounts) ? res.body.accounts : null ;
            this.groups = (res.body.groupDetails) ? res.body.groupDetails : null ;
            this.total = res.body.closingBalance.amount;
            this.previous = '';
            this.current = '';
            //this.storeChartData(this.groups);
            this.flattenTB(res.body.groupDetails, 'root');            
          }, 
          err => {            
            console.log(err);
        });
      });
    });   
  }
 
  ngOnInit(): void {
    this.storage.get('companyName').then((res) => {
      this.companyName = res;
    })
    if(!this.viewCtrl.index){
      this.getTB();      
    }
    else{
      this.accounts = this.navParams.get('accounts');
      this.groups = this.navParams.get('groups');
      this.total = this.navParams.get('total');
      this.previous = this.navParams.get('previous');
      this.current = this.navParams.get('current');
    }
  }

  itemTapped(data) {
    console.log(data);
    this.navCtrl.push(TBalance, {
      'accounts': data.accounts,
      'groups': data.childGroups,
      'total': data.closingBalance.amount,
      'previous': data.parent,
      'current': data.groupName
    });    
  }

  goToHome(){
    this.navCtrl.popToRoot();
  }

  /* storeChartData(data){
    for(let x in data){
      if(data[x].category == 'income'){
        this.chartData['']
      }
      console.log(data[x]);
    }
   //this.storage.set('piechart', res.body.authKey); 
  } */
  
  getItems(ev: any) {
    // set val to the value of the searchbar
    let val = ev.target.value;
    let ac=[], grp=[];
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.flatten.filter((item) => {
        if(item.query.toLowerCase().indexOf(val.toLowerCase()) > -1){
          if('groupName' in item){
            grp.push(item);            
          }
          if('name' in item){
            ac.push(item);            
          }
        };
      })
      this.searching = true;
      this.accounts = ac;
      this.groups = grp;
    }
    else
    {
      this.searching = false;
      this.accounts = this.groupsAndAccounts.accounts;
      this.groups = this.groupsAndAccounts.groupDetails;  
    }
  }

  sort(property){
    if(property == 'name'){
      this.columnGroup = 'groupName';
      this.columnAccount = 'name';
    }
    else{
      this.columnGroup = 'closingBalance.amount';
      this.columnAccount = 'closingBalance.amount';
    }    
    this.direction = (this.direction == 'asc') ? 'desc' : 'asc'; 
  };

  cancelSearch(){
    this.searching = false;
  }
  
  flattenTB(data, parent){
    for(let x in data){

      if(data[x].uniqueName == 'sundrycreditors'){
        this.sundrycreditors = true;
      }
      else if(data[x].uniqueName == 'sundrydebtors'){
        this.sundrydebtors = true;
      }

      if(data[x].category == 'liabilities' && this.sundrycreditors){
        this.category = 'sundrycreditors';        
      }
      else if(data[x].category == 'assets' && this.sundrydebtors){
        this.category = 'sundrydebtors';        
      }
      else{
        if(data[x].category)
          this.category = data[x].category;
      };

      data[x]['understanding'] = Constant.UNDERSTANDING[ this.category ][ data[x].closingBalance.type ];
      data[x]['odd'] = Constant.UNDERSTANDING[ this.category ][ 'odd' ];
      data[x]['parent'] = parent;

      if('groupName' in data[x]) data[x]['query'] = data[x].groupName;
      else if('name' in data[x]) data[x]['query'] = data[x].name;

      this.flatten.push(data[x]);      
      if('childGroups' in data[x] && data[x].childGroups.length > 0){
        this.flattenTB(data[x].childGroups, data[x].groupName);
      }
      if('accounts' in data[x] && data[x].accounts.length > 0){
        this.flattenTB(data[x].accounts, data[x].groupName);
      }      
    }
    
  }

}