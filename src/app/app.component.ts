import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events, LoadingController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import { Common } from '../shared/common';

import { LoginPage } from '../pages/login/login';
import { Dashboard } from '../pages/dashboard/dashboard';
import { Company } from '../pages/company/company';
import { TBalance } from '../pages/tbalance/tbalance';

import { TranslateService } from '@ngx-translate/core'
import { SpinnerService } from '../shared/spinner.service';
import { RootPageService } from '../shared/rootPage.service';
import { InfoService } from "../shared/info.service";

import { SessionService } from "../shared/session.service";
import { LoginService } from '../pages/login/login-service';
import { companyService } from "../pages/company/company.service";
import { GooglePlus } from '@ionic-native/google-plus';

import firebase from 'firebase';


@Component({
  templateUrl: 'app.html',
  providers: [Common,companyService,LoginService]
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage:any;
  pages: Array<{title: string, component: any}>;
  userEmail: string;
  companyName: string;
  loading: boolean = false;
  testName: string = "Ashish Yadav";
  public userProfile:any = null;
  token:any = null;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    private translate: TranslateService,
    private storage: Storage,
    public cmn: Common,
    public spinnerService: SpinnerService,
    public rootPageService: RootPageService,
    public infoService: InfoService,
	public events: Events,
	private googlePlus : GooglePlus,
    private loginService: LoginService,
    public sessionService: SessionService,
    private companyService: companyService,
	public common: Common,
	private loader : LoadingController
  ) {
    const config = {
		apiKey: "AIzaSyC032wI40re22C0S_PYdMwgkwVG1XKT3wY",
		authDomain: "fir-giddh.firebaseapp.com",
		databaseURL: "https://fir-giddh.firebaseio.com",
		projectId: "firebase-giddh",
		storageBucket: "firebase-giddh.appspot.com",
		messagingSenderId: "519364484616"
	};

    this.initializeApp();
    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Dashboard', component: Dashboard },
      { title: 'Switch Company', component: Company },
      { title: 'Trial Balance', component: TBalance },
      { title: 'Logout', component: LoginPage }
    ];

    firebase.initializeApp(config);
  }

  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang('en');

    if (this.translate.getBrowserLang() !== undefined) {
      this.translate.use(this.translate.getBrowserLang());
    } else {
      this.translate.use('en'); // Set your language here
    }
  }

  initializeApp() {
    this.platform.ready().then(() => {
      console.log("initializeApp");


      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.spinnerService.spinner.subscribe((val) => {
        this.loading = val;
      })

      this.rootPageService.rootPage.subscribe((val) => {
        this.rootPage = val;
      })

      this.infoService.companyName.subscribe((val) => {
        this.companyName = val;
      })

      this.infoService.userEmail.subscribe((val) => {
        this.userEmail = val;
      })

      this.storage.get('companyName').then((res) => {
        this.companyName = res;
      })
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.checkAuth();
      this.initTranslate();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
	// we wouldn't want the back button to show in this scenario

	let loader = this.loader.create();
	if(page.title == 'Logout'){
		loader.present().then(()=>{
			this.googlePlus.logout().then(()=>{
				loader.dismiss();
				this.nav.setRoot(page.component);
			});
		});	
	}else{
		this.nav.setRoot(page.component);
	}
    
  }

  checkAuth(){
    this.storage.get('sessionId').then((val) => {
      if(val){
        this.storage.get('user').then((res) => {
          this.userEmail = res.email;
          this.rootPage = TBalance;
        })
      }else
      {
        this.rootPage = LoginPage;
      }
    });
  }

}
