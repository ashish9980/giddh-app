import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule, LoadingController } from 'ionic-angular';
import { HttpModule, Http } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { Storage } from '@ionic/storage';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { GooglePlus } from '@ionic-native/google-plus';

import { MyApp } from './app.component';
import { LoginPage } from '../pages/login/login';
import { Company } from '../pages/company/company';
import { Dashboard } from '../pages/dashboard/dashboard';
import { TBalance } from '../pages/tbalance/tbalance';

import { LoginService } from '../pages/login/login-service';

import { OrderBy } from '../shared/orderby.pipe';
import { Common } from '../shared/common';
import { SpinnerService } from '../shared/spinner.service';
import { RootPageService } from '../shared/rootPage.service';
import { HttpInterceptorModule, HttpInterceptorService, getHttpHeadersOrInit } from 'ng-http-interceptor';
import { SessionService } from "../shared/session.service";
import { InfoService } from "../shared/info.service";
import { AngularFireModule } from 'angularfire2';
import { LinkedIn } from '@ionic-native/linkedin';


export function HttpLoaderFactory(http: Http) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    Dashboard,
    Company,
    TBalance,
    OrderBy
  ],
  imports: [
    BrowserModule,
    HttpInterceptorModule,
    IonicModule.forRoot(MyApp),
    //IonicPageModule.forChild(LoginPage),
    IonicStorageModule.forRoot(),
    HttpModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [Http]
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    Dashboard,
    Company,
    TBalance
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    LoginService,
    Common,
    SpinnerService,
    SessionService,
    RootPageService,
    InfoService,
    GooglePlus,
    LinkedIn
  ]
})
export class AppModule {
  requests = [];
  res = null;
  error = null;
  loading: any;
  sessionId: any;

  constructor(
    private httpInterceptor: HttpInterceptorService,
    public loadingCtrl: LoadingController,
    public spinnerService: SpinnerService,
    public sessionService: SessionService,
    public rootPageService: RootPageService,
    public storage: Storage,
    public common: Common,
    public http: Http
  ) {

    this.httpInterceptor.request().addInterceptor((data, method) => {
      const headers = getHttpHeadersOrInit(data, method);
      if(!this.sessionId){
        this.sessionService.session.subscribe((val)=>{
          this.sessionId = val;
        })
      }
      else{
        headers.set('Session-Id', this.sessionId);
      }

      this.error = null;
      this.requests.push({
        method: method,
        url: data[0]
      });

      this.spinnerService.setSpinner(true);
      console.log('request: ', data);
      return data;
    });

    this.httpInterceptor.response().addInterceptor(res => res.do(
      x => {
        console.log(x);
        this.spinnerService.setSpinner(false);
      },
      e => {
        this.error = e;
        this.spinnerService.setSpinner(false);
        let body = JSON.parse(e._body);
        this.common.showMsg('error', e);
        console.log(e, body);
        if(body.code === "SESSION_EXPIRED_OR_INVALID"){
          this.rootPageService.setRootPage(LoginPage);
        }
      }
    ));
  }

}
