export const API_URL= 'http://api.giddh.com';
export const API_URL_V2= 'http://api.giddh.com/v2';

// export const API_URL= 'http://apitest.giddh.com';
// export const API_URL_V2= 'http://apitest.giddh.com/v2';

// export const API_URL= 'http://apidev.giddh.com';
// export const API_URL_V2= 'http://apidev.giddh.com/v2';


export const UNDERSTANDING: any = {
    sundrycreditors:{
        CREDIT: 'Payable to creditors',
        DEBIT: 'Advance to creditors',
        odd: 'DEBIT'
    },
    sundrydebtors:{
        CREDIT: 'Advance from Debtors',
        DEBIT: 'Due',
        odd: 'CREDIT'
    },
    liabilities: {
        CREDIT: 'Liability Payable',
        DEBIT: 'Liabilities paid in advance',
        odd: 'DEBIT'
    },
    assets: {
        CREDIT: '(-) Asset',
        DEBIT: 'Asset value',
        odd: 'CREDIT'
    },
    expenses: {
        CREDIT: 'Negative Expense',
        DEBIT: 'Expense',
        odd: 'CREDIT'
    },
    income: {
        CREDIT: 'Revenue',
        DEBIT: '(-) Negative Revenue',
        odd: 'DEBIT'
    }
}
