import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Storage } from '@ionic/storage';

@Injectable()
export class SessionService{
    public session = new Subject<any>();

    constructor(
        public storage: Storage
    ) {
        this.storage.get('sessionId').then((val) => {
            this.session.next(val);            
        })
    }
    
    public setSession(val){        
        this.session.next(val);        
    }
}