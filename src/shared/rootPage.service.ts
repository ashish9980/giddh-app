import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class RootPageService{
    public rootPage = new Subject<any>();

    public setRootPage(val){
        this.rootPage.next(val);
    }
}