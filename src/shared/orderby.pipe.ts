import { Pipe, PipeTransform } from '@angular/core';
import * as _ from "lodash";

@Pipe({
  name: 'OrderBy'
})

export class OrderBy implements PipeTransform {
  transform(array: any, args?: any): any {
    return _.orderBy(array, [args.property],[args.direction])
  }
}