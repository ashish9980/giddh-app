import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class SpinnerService{
    public spinner = new Subject<any>();

    public setSpinner(val){
        this.spinner.next(val);
    }
}