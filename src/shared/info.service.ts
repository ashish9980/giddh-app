import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class InfoService{
    public userEmail = new Subject<any>();
    public companyName = new Subject<any>();

    public setUserEmail(val){
        this.userEmail.next(val);
    }

    public setCompany(val){
        this.companyName.next(val);
    }
}