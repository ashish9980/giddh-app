import { Injectable } from '@angular/core';
import { LoadingController, Loading, AlertController} from 'ionic-angular';

@Injectable()
export class Common {
    loading: Loading;
    constructor(
        public loadingCtrl: LoadingController,
        private alertCtrl: AlertController
    ){}

    public showLoading() {
        this.loading = this.loadingCtrl.create({
        content: 'Please wait...',
        dismissOnPageChange: false
        });
        this.loading.present();
    }

    public dismissLoading(){        
        this.loading.dismiss();                
    }
 
    public showMsg(status, msg) {
        let alert = this.alertCtrl.create({
        title: status,
        subTitle: msg,
        buttons: ['OK']
        });
        alert.present(prompt);
    }
}
